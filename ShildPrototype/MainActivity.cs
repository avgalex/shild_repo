﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;

namespace ShildPrototype
{
    [Activity (Label = "Щит", MainLauncher = true, Icon = "@drawable/shield_green", Theme = "@android:style/Theme.Holo.Light")]
	public class MainActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);
			Button btInput = FindViewById<Button> (Resource.Id.btInput);

			btInput.Click += (sender, e) => 
			{
				
				var intent = new Intent (this, typeof(ChoiseOrganizationActivity));
				StartActivity (intent);
			};		
			

		
		}
	}
}


