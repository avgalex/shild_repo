﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ShildPrototype
{
    public class DocumentsListAdapter:BaseAdapter<DocumentsModel>
    {
        Activity mainActivity;
        List<DocumentsModel> lstDocumentsInfo;

        public DocumentsListAdapter(Activity mainActivity, List<DocumentsModel> lstFlimStarInfo)
        {
            this.mainActivity = mainActivity;
            this.lstDocumentsInfo = lstFlimStarInfo;
        }

        #region implemented abstract members of BaseAdapter
        public override long GetItemId (int position)
        {
            return position;
        }
        public override View GetView (int position, View convertView, ViewGroup parent)
        {
            var currentObj = this [position];

            if (convertView == null)
            {
                convertView = mainActivity.LayoutInflater.Inflate(Android.Resource.Layout.ActivityListItem, null);
            }

            convertView.FindViewById<TextView> (Android.Resource.Id.Text1).Text = currentObj.Header;
          //  convertView.FindViewById<ImageView> (Android.Resource.Id.Icon).SetImageResource(currentObj.FlimStarIconImageID);

            return convertView;
        }

        public override int Count {
            get 
            {
                return lstDocumentsInfo != null ? lstDocumentsInfo.Count : -1;
            }
        }
        #endregion

        #region implemented abstract members of BaseAdapter
        public override DocumentsModel this [int index] 
        {
            get 
            {
                return lstDocumentsInfo != null ? lstDocumentsInfo[index] : null;
            }
        }
        #endregion
    }
}

