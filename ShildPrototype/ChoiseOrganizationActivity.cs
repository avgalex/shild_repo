﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ShildPrototype
{
    [Activity (Label = "@string/btInput", Icon = "@drawable/shield_green", Theme = "@android:style/Theme.Holo.Light")]
	public class ChoiseOrganizationActivity :  Activity
	{
		
        List<DocumentsModel> lstDocumentsInfo = null;

		protected override void OnCreate (Bundle bundle)
		{
			//List<string> organization = new List<string>(){"ЗАГС", "ФМС", "МВД", "Пенсионный фонд" };

			base.OnCreate (bundle);
			SetContentView(Resource.Layout.ChoiseOrganitation);

            CreateProjectDS();

			var listOrg = FindViewById<ListView> (Resource.Id.listView1);

            if (listOrg != null)
            {
                listOrg.Adapter = new DocumentsListAdapter(this, lstDocumentsInfo);
                listOrg.ItemClick += OnClickDocumentsItem;
                    
            }
		
			//  listOrg.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, organization);

		}

        const int MY_RESULT_CODE = 101;

        void OnClickDocumentsItem (object sender, AdapterView.ItemClickEventArgs e)
        {
            Intent myIntent = new Intent (this, typeof(ListDocumentsActivity));
            var cItem = lstDocumentsInfo[e.Position];
            myIntent.PutExtra ("Header", cItem.Header);
            myIntent.PutStringArrayListExtra("Documents", cItem.Documents);
           
            StartActivityForResult(myIntent,MY_RESULT_CODE);

        }

        void CreateProjectDS ()
        {
            lstDocumentsInfo = new List<DocumentsModel>();

            var item = new DocumentsModel();
            item.Header = "ФМС";
            item.Documents = new List<string>(){"Гражданский паспорт", "Заграничный паспорт","Миграционная карта"};
            lstDocumentsInfo.Add(item);


            item = new DocumentsModel();
            item.Header = "Пенсионный фонд";
            item.Documents = new List<string>(){"Страховое свидетельство", "Свидетельство ИНН","Анкета", "Договор"};
            lstDocumentsInfo.Add(item);

        }
	}
}

