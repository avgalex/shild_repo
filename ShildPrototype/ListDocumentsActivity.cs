﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ShildPrototype
{
    [Activity (Label ="" , Icon = "@drawable/shield_green", Theme = "@android:style/Theme.Holo.Light")]
	public class ListDocumentsActivity :  Activity
	{	

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);
            SetContentView (Resource.Layout.ListDocumentsView);

           // Button btnGoBack = FindViewById<Button> (Resource.Id.btnGoBack);

//            if(btnGoBack!=null)
//                btnGoBack.Click += (object sender, EventArgs e) => 
//                {
//                    Intent myIntent = new Intent();
//                    myIntent.PutExtra("returnData", this.Intent.GetStringExtra("FilmStarName"));
//                    SetResult(Result.Ok,myIntent);
//                    Finish();
//                };

        }


        protected override void OnStart ()
        {
            base.OnStart ();
            UpdateActivityWithInformation ();
        }

         

       
        void UpdateActivityWithInformation ()
        {
            var txtFilmStarInfo = FindViewById<TextView> (Resource.Id.textViewDoc);
            var lstDoc = FindViewById<ListView>(Resource.Id.listView2);  

            txtFilmStarInfo.Text = this.Intent.GetStringExtra("Header");

            IList<string> documents = this.Intent.GetStringArrayListExtra("Documents");

            lstDoc.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, documents);

            ActionBar.Title = "Документы";

        }
	}
}

